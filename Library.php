<?php
include 'Book.php';

class Library
{
    private $books;

    public function __construct()
    {
        $this->books = array(
            new Book('Archmage', '2002' ),
            new Book('Harry Potter', '1999'),
            new Book('Mein Kampf', '1925'),
            new Book('Archmage 2', '2002' ),
        ) ;
    }

    public function getBooksCountByYear($year){
        $count = 0;

        foreach ($this->books as $book){
            if($book->getYear() == $year){
                $count++;
            }
        }
        return $count;
    }

    public function getBooksCount($books){

        $koll=count($books);

        return $koll;
    }


}